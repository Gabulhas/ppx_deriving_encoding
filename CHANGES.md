## [0.3.0](https://gitlab.com/o-labs/ppx_deriving_encoding/-/compare/0.2.3...0.3.0) (2022-04-11)

* inherit for variant
* camel/snake case flag
* set and map attributes
* remove singleton warning
* better tuple variant
* title and description for cases
* unit argument for mutually recursive types

## [0.2.3](https://gitlab.com/o-labs/ppx_deriving_encoding/-/compare/0.2.2...0.2.3) (2021-03-30)

* Fixes
* Linking with external modules implementing json_encoding
* Relaxing ppxlib constraint

## [0.2.2](https://gitlab.com/o-labs/ppx_deriving_encoding/-/compare/0.2.1...0.2.2) (2021-01-25)

* Switch to json-data-encoding

## [0.2.1](https://gitlab.com/o-labs/ppx_deriving_encoding/-/compare/0.2...0.2.1) (2020-12-18)

* Fix ppxlib dependency

## [0.2](https://gitlab.com/o-labs/ppx_deriving_encoding/-/compare/0.1...0.2) (2020-12-16)

* Switch to ocaml-migrate-parsetree 2.0

## 0.1 (2020-12-15)

First Release
