open Ppxlib
open Ppx_deriving_encoding_lib

let str_gen ~loc ~path:_ (rec_flag, l) enum ign mu force_debug rm_prefix option
    title description schema name modu camel snake pascal kebab wrap assoc =
  Encoding.structure ~loc ~enum ~ign ~mu ~force_debug ?rm_prefix ?option ?title
    ?description ?schema ?name ?modu ~camel ~snake ~pascal ~kebab ~wrap
    ~rec_flag ~assoc l

let sig_gen ~loc ~path:_ (_rec_flag, l) name modu =
  Encoding.signature ~loc ?name ?modu l

let eprefix t =
  let f = Deriving.Args.to_func t in
  Deriving.Args.of_func (fun ctx loc x k ->
      match Utils.rm_prefix_of_expr x with
      | None -> Utils.raise_error ~loc "wrong boolean argument"
      | Some x -> f ctx loc x k)

let () =
  let args_str =
    Deriving.Args.(
      empty +> flag "enum" +> flag "ignore" +> flag "recursive" +> flag "debug"
      +> arg "remove_prefix" (eprefix __)
      +> arg "option" (estring __)
      +> arg "title" __ +> arg "description" __ +> arg "schema" __
      +> arg "name" (estring __)
      +> arg "module_name" (estring __)
      +> flag "camel" +> flag "snake" +> flag "pascal" +> flag "kebab"
      +> flag "wrap" +> flag "assoc")
  in
  let args_sig =
    Deriving.Args.(
      empty +> arg "name" (estring __) +> arg "module_name" (estring __))
  in
  let str_type_decl = Deriving.Generator.make args_str str_gen in
  let sig_type_decl = Deriving.Generator.make args_sig sig_gen in
  Deriving.ignore @@ Deriving.add "encoding" ~str_type_decl ~sig_type_decl;
  Deriving.ignore @@ Deriving.add "data_encoding" ~str_type_decl ~sig_type_decl
