all: build

build:
	@dune build src

dev:
	@dune build

clean:
	@dune clean
